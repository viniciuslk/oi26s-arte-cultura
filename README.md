<h3>Processo de instalacao:</h3>
<hr />

<h5>Baixar o projeto: </h5>
```git clone https://viniciuslk@bitbucket.org/viniciuslk/oi26s-arte-cultura.git```


<h5>Renomear: </h5>
```.env.example para .env```

<h5>Executar docker-compose: </h5>
 ```docker-compose up -d```
 

<h5>Acessar a maquina docker: </h5>
```docker exec -it oi26s-arte-cultura-app bash```

<h5>Acessar pasta e executar o composer:</h5>
```cd /srv/app && composer install && php artisan migrate:refresh```

<hr />

<h3>Requisitos</h3>
<ul>
    <li>Docker version 18.06.1-ce, build e68fc7a ou superior.</li>
</ul>

<h3>Padroes para desenvolvimento: </h3>

<ul>
  <li>Utilizar <strong>Git Flow</strong> para o desenvolvimento de um novo módulo</li>  
</ul>


<h4>Problemas conhecidos</h4>
<ul>
    <li>
        Container do banco de dados, em alguns casos, não pega IP:
        <ul>
            <li>
                Verificar erro: <code>docker-compose ps</code>
            </li>
            <li>
                Corrigir: <code>docker-compose up -d --force-recreate</code>
            </li>
        </ul>
    </li>
</ul>