<?php namespace App\Http\Controllers;

use App\About;
use App\User;
use Illuminate\Support\Facades\Session;
use Input;
use Hash;
use Auth;

class AboutController extends Controller
{

    public function __construct()
    {
    }

    public function index()
    {
//        $itens = About::paginate(15);

        return view('painel.about.index');
    }

    public function save($id)
    {
        $about = About::find($id);
        $about->description = Input::get('description');
        $about->save();
        return redirect('painel/sobre')->with('success', 'Registro alterado com sucesso!');
    }

    public function find()
    {
        $about = About::all()->first();
        return view('painel.about.index', compact('about'));
    }

}
