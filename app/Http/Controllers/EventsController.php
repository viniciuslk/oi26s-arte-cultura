<?php namespace App\Http\Controllers;

use App\Events;
use App\Services\UploadService;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Storage;
use Input;

class EventsController extends Controller
{

    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    public function index()
    {

        $events = Events::all();

        return view('painel.events.index', compact('events'));
    }

    public function create() {
        return view('painel.events.create');
    }

    public function save(Request $request)
    {
        $events = new Events();

        $events->title = Input::get('title');
        $events->subtitle = Input::get('subtitle');
        $events->content = Input::get('content');

        if($request->hasFile('image')) {
            $events->img_url = $this->uploadService->uploadImageToS3Bucket($request->file('image'));
        }

        if($request->hasFile('event-guideline')) {
            $events->guideline_url = $this->uploadService->uploadPDFToS3Bucket($request->file('event-guideline'));
        }

        $events->save();
        return redirect('painel/eventos')->with('success', 'Registro gravado com sucesso!');
    }

    public function update($id, Request $request)
    {
        $events = Events::find($id);

        $events->title = Input::get('title');
        $events->subtitle = Input::get('subtitle');
        $events->content = Input::get('content');

        if($request->hasFile('image')) {
            $events->img_url = $this->uploadService->uploadImageToS3Bucket($request->file('image'));
        }

        if($request->hasFile('event-guideline')) {
            $events->guideline_url = $this->uploadService->uploadPDFToS3Bucket($request->file('event-guideline'));
        }

        $events->save();
        return redirect('painel/eventos')->with('success', 'Registro gravado com sucesso!');
    }

    public function find($id)
    {
        $events = Events::find($id);
        return view('painel.events.update', compact('events'));
    }

    public function delete($id) {
        Events::destroy($id);
        return redirect('painel/eventos');
    }

}
