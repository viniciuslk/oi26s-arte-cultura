<?php namespace App\Http\Controllers;

use App\News;
use App\Services\UploadService;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Input;
use Storage;


class NewsController extends Controller
{


    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    public function index()
    {

        $news = News::all();

        return view('painel.news.index', compact('news'));
    }

    public function create() {
        return view('painel.news.create');
    }

    public function save(Request $request)
    {
        $news = new News();

        $news->title = Input::get('title');
        $news->subtitle = Input::get('subtitle');
        $news->content = Input::get('content');

//        dd($request->hasFile('image'));
        if($request->hasFile('image')) {
            $news->img_url = $this->uploadService->uploadImageToS3Bucket($request->file('image'));
        }

        $news->save();
        return redirect('painel/noticias')->with('success', 'Registro gravado com sucesso!');
    }

    public function update($id, Request $request)
    {
        $news = News::find($id);

        $news->title = Input::get('title');
        $news->subtitle = Input::get('subtitle');
        $news->content = Input::get('content');

        if($request->hasFile('image')) {
            $news->img_url = $this->uploadService->uploadImageToS3Bucket($request->file('image'));
        }

        $news->save();
        return redirect('painel/noticias')->with('success', 'Registro gravado com sucesso!');
    }

    public function find($id)
    {
        $news = News::find($id);
        return view('painel.news.update', compact('news'));
    }

    public function delete($id) {
        News::destroy($id);
        return redirect('painel/noticias');
    }

}
