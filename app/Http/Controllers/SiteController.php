<?php namespace App\Http\Controllers;

use App\About;
use App\Events;
use App\News;
use Illuminate\Support\Facades\Hash;

class SiteController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $news = News::all();
        $about = About::all();
        $events = Events::all();

		return view('home', compact(['about', 'events', 'news']));
	}

	public function produtos(){
	    $produtos = Produtos::where('liberado', '=', '1');

	    return view('produtos', compact('produtos'));
    }
}
