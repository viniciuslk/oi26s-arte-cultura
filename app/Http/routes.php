<?php


use Illuminate\Support\Facades\Route;

Route::get('/', 'SiteController@index');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::group(['prefix' => 'painel', 'middleware' => 'auth'], function () {

    Route::get('/', 'DashboardController@index');

    Route::group(['prefix' => 'usuarios'], function () {
        Route::get('/', 'UsuariosController@index');
        Route::get('create', 'UsuariosController@create');
        Route::post('create', 'UsuariosController@create2');
        Route::get('update', 'UsuariosController@update');
        Route::post('update', 'UsuariosController@update2');
        Route::get('destroy', 'UsuariosController@destroy');
    });

    Route::group(['prefix' => 'sobre'], function () {
        Route::get('/', 'AboutController@find');
        Route::post('{id}', 'AboutController@save');
    });

    Route::group(['prefix' => 'noticias'], function () {
        Route::get('/', 'NewsController@index');
        Route::get('nova', 'NewsController@create');
        Route::post('save', 'NewsController@save');
        Route::get('editar/{id}', 'NewsController@find');
        Route::post('update/{id}', 'NewsController@update');
        Route::get('delete/{id}', 'NewsController@delete');
    });

    Route::group(['prefix' => 'eventos'], function () {
        Route::get('/', 'EventsController@index');
        Route::get('nova', 'EventsController@create');
        Route::post('save', 'EventsController@save');
        Route::get('editar/{id}', 'EventsController@find');
        Route::post('update/{id}', 'EventsController@update');
        Route::get('delete/{id}', 'EventsController@delete');
    });

});
