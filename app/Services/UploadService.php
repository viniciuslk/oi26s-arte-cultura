<?php
namespace App\Services;

use Illuminate\Support\Facades\Storage;

class UploadService
{
    public function uploadImageToS3Bucket($file) {
        $imageFileName = time() . '.' . $file->getClientOriginalExtension();
        $s3 = Storage::disk('s3');
        $filePath = '/images/' . $imageFileName;
        $s3->put($filePath, file_get_contents($file), 'public');
        return env('S3_BASE_URL').'/'.env('S3_BUCKET').$filePath;
    }

    public function uploadPDFToS3Bucket($file) {
        $imageFileName = time() . '.' . $file->getClientOriginalExtension();
        $s3 = Storage::disk('s3');
        $filePath = '/pdf/' . $imageFileName;
        $s3->put($filePath, file_get_contents($file), 'public');
        return env('S3_BASE_URL').'/'.env('S3_BUCKET').$filePath;
    }
}
