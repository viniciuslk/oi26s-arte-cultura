<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Painel - Laravel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'>
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset("bootstrap/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset("css/app.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!-- Website Assets -->
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('website/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/nivo-lightbox.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/responsive.css')}}">


</head>
<body>
    {{--@include("includes.top")--}}
    @yield('content')

    <!-- jQuery 2.1.4 -->
    <script src="{{asset("plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset("bootstrap/js/bootstrap.min.js")}}"></script>

    <!-- WEBSITE JS -->
    <script src="{{asset('website/js/jquery-min.js')}}"></script>
    <script src="{{asset('website/js/popper.min.js')}}"></script>
    <script src="{{asset('website/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('website/js/jquery.mixitup.js')}}"></script>
    <script src="{{asset('website/js/nivo-lightbox.js')}}"></script>
    <script src="{{asset('website/js/owl.carousel.js')}}"></script>
    <script src="{{asset('website/js/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('website/js/jquery.nav.js')}}"></script>
    <script src="{{asset('website/js/scrolling-nav.js')}}"></script>
    <script src="{{asset('website/js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('website/js/smoothscroll.js')}}"></script>
    <script src="{{asset('website/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('website/js/wow.js')}}"></script>
    <script src="{{asset('website/js/jquery.vide.js')}}"></script>
    <script src="{{asset('website/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('website/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('website/js/waypoints.min.js')}}"></script>
    <script src="{{asset('website/js/form-validator.min.js')}}"></script>
    <script src="{{asset('website/js/contact-form-script.js')}}"></script>
    <script src="{{asset('website/js/main.js')}}"></script>

    <!-- page script -->
    <script>
        $(function () {
            // Scripts aqui
        })
    </script>

</body>
</html>
