{{--@extends('app')--}}
@section('content')
    <!-- Header Section Start -->
    <header id="hero-area" data-stellar-background-ratio="0.5">
        <div class="hero-area-blurred"></div>
        <!-- Navbar Start -->
        <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a href="#" class="navbar-brand"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar"
                            aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="lnr lnr-menu"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="main-navbar">
                    <ul class="navbar-nav mr-auto w-100 justify-content-end">
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#hero-area">Início</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#events">Eventos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#blog">Notícias</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#about">Sobre</a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Mobile Menu Start -->
            <ul class="mobile-menu">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#home">Início</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#events">Eventos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#blog">Notícias</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#about">Sobre</a>
                </li>
            </ul>
            <!-- Mobile Menu End -->

        </nav>
        <!-- Navbar End -->
        <div class="container hero-area-image-container">
            <div class="row justify-content-md-center">
                <div class="col-md-10">
                    <div class="contents text-center">
                        <img src="{{asset('imgs/logo.png')}}" alt="logo"/>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Section End -->

    {{-- Events section --}}
    <section id="events" class="section">
        <!-- Container Starts -->
        <div class="container">
            <div class="section-header">
                <h2 class="section-title">Nossos eventos</h2>
                <hr class="lines">
                <p class="section-subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat,
                    dignissimos! <br> Lorem ipsum dolor sit amet, consectetur.</p>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- Portfolio Controller/Buttons -->
                    <div class="controls text-center">
                        <a class="filter active btn btn-common" data-filter="all">
                            TODOS
                        </a>
                        @foreach($events as $eventsItem)
                            <a class="filter active btn btn-common" data-filter=".{!! preg_replace('/\W+/', '-', strtolower($eventsItem->title)); !!}">
                                {{ $eventsItem->title }}
                            </a>
                        @endforeach
                    </div>
                    <!-- Portfolio Controller/Buttons Ends-->
                </div>

                <!-- Portfolio Recent Projects -->
                <div id="portfolio" class="row">
                    @foreach($events as $eventsItem)
                        <div class="col-xs-12 col-sm-6 col-md-4 mix {!! preg_replace('/\W+/', '-', strtolower($eventsItem->title)); !!}"
                             id="{!! preg_replace('/\W+/', '-', strtolower($eventsItem->title)); !!}">
                            <div class="portfolio-item">
                                <div class="shot-item">
                                    <img src="{{$eventsItem->img_url}}" class="portfolio-item--img" alt=""/>
                                    <a class="overlay lightbox" href="{{$eventsItem->img_url}}">
                                        <i class="lnr lnr-eye item-icon"></i>
                                    </a>
                                </div>
                                <div class="text-justify portfolio-item--subtitle">
                                    {{$eventsItem->subtitle}}
                                </div>
                                <div class="d-flex">
                                    <i class="mdi mdi-file-pdf"></i>
                                    <a target="_blank" href="{{$eventsItem->guideline_url}}">Consulte o edital deste
                                        evento!</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Container Ends -->
    </section>
    {{-- Events section --}}

    {{-- News section --}}
    <section id="blog" class="section">
        <!-- Container Starts -->
        <div class="container">
            <div class="section-header">
                <h2 class="section-title">Últimas Notícias</h2>
                <hr class="lines">
                <p class="section-subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat,
                    dignissimos! <br> Lorem ipsum dolor sit amet, consectetur.</p>
            </div>
            <div class="row">
                @foreach($news as $newsItem)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 blog-item">
                        <!-- Blog Item Starts -->
                        <div class="blog-item-wrapper">
                            <div class="blog-item-img">
                                <a href="single-post.html">
                                    <img src="{{$newsItem->img_url}}" alt="">
                                </a>
                            </div>
                            <div class="blog-item-text">
                                <div class="meta-tags">
                                    <span class="date"><i class="lnr  lnr-clock"></i>2 Days Ago</span>
                                    <span class="comments"><a href="#"><i
                                                    class="lnr lnr-bubble"></i> 24 Comments</a></span>
                                </div>
                                <h3>
                                    <a href="single-post.html">{{$newsItem->title}}</a>
                                </h3>
                                <p>{{$newsItem->subtitle}}</p>
                                <a href="single-post.html" class="btn-rm">Ler mais <i
                                            class="lnr lnr-arrow-right"></i></a>
                            </div>
                        </div>
                        <!-- Blog Item Wrapper Ends-->
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    {{-- News section --}}

    {{-- About section --}}
    <section class="video-promo section" id="about">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="video-promo-content text-center">
                        <h2 class="wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Sobre...</h2>
                        <p class="wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">
                            {{ $about[0]['description'] }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- About section --}}
@endsection
