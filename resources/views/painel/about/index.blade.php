@extends('painel.painel-layout')
@section('content-layout')
    @if (session('success'))
        <script>
            $.toast({
                text : "Registro alterado com sucesso!",
                showHideTransition : 'slide',
                bgColor : '#0e7f59',
                textColor : '#eee',
                allowToastClose : false,
                hideAfter : 5000,
                stack : 5,
                textAlign : 'right',
                position : 'top-right'
            })
        </script>
    @endif
    <div class="card">
        <div class="card-body">
            <h3 class="">Sobre</h3>
            <p class="card-description">
                Que tal descrever um pouco sobre a comissão?
                Utilize este campo abaixo e fale tudo o que você acha interessante compartilhar na página do site!
            </p>

            <form action="{{url('painel/sobre/'.$about->id)}}" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <textarea name="description" id="maxlength-textarea" class="form-control" rows="5" placeholder="Sobre...">{{ $about->description }}</textarea>
                </div>
                <input class="btn btn-primary" type="submit" value="Gravar">
                <input class="btn btn-light" type="reset" value="Cancelar">
            </form>
        </div>
    </div>
@endsection