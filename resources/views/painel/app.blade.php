<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Laravel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset("bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    {{-- Admin assets --}}
    {{-- CSS --}}
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/vendor.bundle.addons.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/toastr.min.css')}}">

    <!-- jQuery 2.1.4 -->
    <script src="{{asset("plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
    <script src="{{asset("js/scripts.js")}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset("bootstrap/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("admin/js/toastr.min.js")}}"></script>

</head>
<body>

    @yield('content')

    <!-- page script -->
    <script>
        $(function () {

        })
    </script>

</body>
</html>
