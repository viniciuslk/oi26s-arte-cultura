@extends('painel.painel-layout')
@section('content-layout')
    <div class="card">
        <div class="card-body">
            <h4>Cadastro de Eventos</h4>
            <p>Utilize os campos abaixo para cadastrar evento!</p>
            <form action="{{url('painel/eventos/save')}}" enctype="multipart/form-data" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label for="title">Título</label>
                    <input type="text" name="title" class="form-control">
                </div>
                <div class="form-group">
                    <label for="title">Subtítulo</label>
                    <input type="text" name="subtitle" class="form-control">
                </div>
                <div class="form-group">
                    <label for="title">Conteúdo</label>
                    <textarea name="content" class="form-control" rows="15" placeholder="Conteúdo do evento..."></textarea>
                </div>
                <div class="form-group">
                    <label for="title">Imagem</label>
                    <input type="file" name="image" class="form-control">
                </div>
                <div class="form-group">
                    <label for="title">Edital do Evento</label>
                    <input type="file" name="event-guideline" class="form-control">
                </div>
                <div class="form-group">
                    <input class="btn btn-primary" type="submit" value="Gravar">
                    <input class="btn btn-light" type="reset" value="Cancelar">
                </div>
            </form>
        </div>
    </div>
@endsection
