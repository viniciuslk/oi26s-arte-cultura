@extends('painel.painel-layout')
@section('content-layout')
    <div class="card">
        <div class="card-body">
            <h4>Cadastro de Eventos</h4>
            <p>Utilize os campos abaixo para cadastrar sua evento!</p>
            <form action="{{url('painel/eventos/update/'.$events->id)}}" enctype="multipart/form-data" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label for="title">Título</label>
                    <input value="{{$events->title}}" type="text" name="title" class="form-control">
                </div>
                <div class="form-group">
                    <label for="title">Subtítulo</label>
                    <input value="{{$events->subtitle}}" type="text" name="subtitle" class="form-control">
                </div>
                <div class="form-group">
                    <label for="title">Conteúdo</label>
                    <textarea name="content" class="form-control" rows="15" placeholder="Conteúdo da evento...">{{ $events->content }}</textarea>
                </div>

                {{-- UPLOAD DA IMAGEM DO EVENTO --}}
                <div class="form-group">
                    <div class="input-file-with-preview__img-container">
                        <a href="{{$events->img_url}}" target="_blank">
                            <img class="input-file-with-preview__img-container--img" src="{{$events->img_url}}"/>
                        </a>
                    </div>
                    <label for="title">Imagem</label>
                    <input type="file" name="image" class="form-control">
                </div>

                {{-- UPLOAD DO EDITAL --}}
                <div class="form-group">
                    <label for="title">Edital do Evento</label>
                    <input type="file" name="event-guideline" class="form-control">
                    <div class="d-flex">
                        <i class="mdi mdi-file-pdf"></i>
                        <a target="_blank" href="{{$events->guideline_url}}">Edital atual</a>
                    </div>
                </div>
                <div class="form-group">
                    <input class="btn btn-primary" type="submit" value="Alterar">
                    <input class="btn btn-light" type="reset" value="Cancelar">
                </div>
            </form>
        </div>
    </div>
@endsection
