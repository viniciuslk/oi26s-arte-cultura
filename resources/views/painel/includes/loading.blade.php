<div class="c-loading">
    <div class="c-loading__spinners">
        <div class="c-loading__spinner c-loading__spinner--two"></div>
            <div class="c-loading__spinner--bounce1"></div>
            <div class="c-loading__spinner--bounce2"></div>
        </div>
    </div>
</div>
