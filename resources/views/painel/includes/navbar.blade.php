<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
            <img src="{{asset('imgs/logo-min.png')}}" alt="logo"/>
            Cultura UTFPR
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
            <img src="{{asset('imgs/logo-min.png')}}" alt="logo"/>
            Arte e Cultura
        </a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center">

    </div>
</nav>
