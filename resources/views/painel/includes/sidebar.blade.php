<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="user-wrapper">
                    <div class="profile-image">
                        <img src="images/faces/face1.jpg" alt="profile image">
                    </div>
                    <div class="text-wrapper">
                        <p class="profile-name">Richard V.Welsh</p>
                        <div>
                            <small class="designation text-muted">Manager</small>
                            <span class="status-indicator online"></span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.html">
                <i class="menu-icon mdi mdi-television"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('painel/eventos')}}">
                <i class="menu-icon mdi mdi-calendar-check"></i>
                <span class="menu-title">Eventos</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('painel/noticias')}}">
                <i class="menu-icon mdi mdi-newspaper"></i>
                <span class="menu-title">Notícias</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('painel/sobre')}}">
                <i class="menu-icon mdi mdi-information-outline"></i>
                <span class="menu-title">Sobre</span>
            </a>
        </li>
    </ul>
</nav>