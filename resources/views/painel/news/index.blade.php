@extends('painel.painel-layout')
@section('content-layout')
    <div class="card">
        <div class="card-body">
            <h3>Notícias</h3>
            <p>Aqui estamos listando todas as notícas, por ordem decresente de data, que você criou!</p>
            <div class="form-group">
                <a href="{{ url('painel/noticias/nova')}}" class="btn btn-primary">Nova notícia</a>
            </div>
            @if(sizeof($news) > 0)
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Título</th>
                        <th>Subtítulo</th>
                        <th width="100px">Criado em</th>
                        <th width="100px">Editado em</th>
                        <th width="100px"></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($news as $item)
                        <tr>
                            <td>{!! $item->title !!}</td>
                            <td>{!! $item->subtitle !!}</td>
                            <td>{!! date('d/m/Y', strtotime($item->created_at)) !!}</td>
                            <td>{!! date('d/m/Y', strtotime($item->updated_at)) !!}</td>
                            <td>
                                <a href="{{ url('painel/noticias/editar/' . $item->id)}}" class="btn btn-warning">
                                    <i class="mdi mdi-pencil"></i>
                                </a>
                                <a href="{{ url('painel/noticias/delete/' . $item->id)}}"class="btn btn-danger">
                                    <i class="mdi mdi-delete"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                @include('painel.includes.not-found-results')
            @endif
        </div>
    </div>
@endsection