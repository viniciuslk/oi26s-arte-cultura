@extends('painel.painel-layout')
@section('content-layout')
    <div class="card">
        <div class="card-body">
            <h4>Cadastro de Notícias</h4>
            <p>Utilize os campos abaixo para cadastrar sua notícia!</p>
            <form action="{{url('painel/noticias/update/' . $news->id )}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label for="title">Título</label>
                    <input value="{{$news->title}}" type="text" name="title" class="form-control">
                </div>
                <div class="form-group">
                    <label for="title">Subtítulo</label>
                    <input value="{{$news->subtitle}}" type="text" name="subtitle" class="form-control">
                </div>
                <div class="form-group">
                    <label for="title">Conteúdo</label>
                    <textarea name="content" class="form-control" rows="15"
                              placeholder="Conteúdo da notícia...">{{ $news->content }}</textarea>
                </div>
                <div class="form-group input-file-with-preview">
                    <div class="input-file-with-preview__img-container">
                        <a href="{{$news->img_url}}" target="_blank">
                            <img class="input-file-with-preview__img-container--img" src="{{$news->img_url}}"/>
                        </a>
                    </div>
                    <label for="title">Imagem</label>
                    <input type="file" name="image" class="form-control">
                </div>
                <div class="form-group">
                    <input class="btn btn-primary" onclick="handleClickLoading()" type="submit" value="Alterar">
                    <input class="btn btn-light" type="reset" value="Cancelar">
                </div>
            </form>
        </div>
    </div>
@endsection
