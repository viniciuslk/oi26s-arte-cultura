@extends('painel.app')

@section('content')
    <div class="container-scroller">

        @include('painel.includes.navbar')

        <div class="container-fluid page-body-wrapper">
            @include('painel.includes.sidebar')
            <div class="main-panel">
                <div class="content-wrapper">
                    @yield('content-layout')
                </div>
            </div>
        </div>

    </div>
@endsection