@extends('painel.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Usuários</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="{{url('painel/usuarios/update?codigo='.Input::get('codigo'))}}" method="POST">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                    <div class="form-group">
                                        <label for="nome">Nome</label>
                                        <input type="text" class="form-control" name="nome" value="{{$item->nome}}"
                                               placeholder="Nome" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="descricao">Descrição</label>
                                        <input type="text" class="form-control" name="descricao" value="{{$item->descricao}}"
                                               placeholder="Nome" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="quantidade">Quantidade</label>
                                        <input type="number" class="form-control" name="quantidade" value="{{$item->quantidade}}"
                                               placeholder="Quantidade" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="preco">Preço</label>
                                        <input type="text" class="form-control" name="preco" value="{{$item->preco}}"
                                               placeholder="Preço" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="liberado">Liberado</label>
                                        <input type="checkbox" name="liberado" value="1" {{$item->liberado == 1 ? 'checked':''}}
                                               placeholder="Liberado">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save">&nbsp;</i>Salvar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
