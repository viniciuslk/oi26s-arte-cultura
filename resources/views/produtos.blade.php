@extends('app')
@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                @foreach($produtos as $produto)
                    <div class="col-md-3">{{$produto->nome}}</div>
                    <div class="col-md-2">{{$produto->descricao}}</div>
                    <div class="col-md-2">{{$produto->quantidade}}</div>
                    <div class="col-md-3">{{$produto->preco}}</div>
                    <div class="col-md-2">{{$produto->liberado}}</div>
                @endforeach
            </div>
        </div>
    </div>
@endsection